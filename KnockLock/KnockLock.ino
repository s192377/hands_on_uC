// the setup routine runs once when you press reset:
const byte interruptPin = 0;
volatile byte state = LOW;
long debouncing_time = 15; //Debouncing Time in Milliseconds
volatile unsigned long last_micros;
void setup() {
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  pinMode(interruptPin, INPUT_PULLDOWN);
  attachInterrupt(digitalPinToInterrupt(interruptPin), debounceInterrupt, RISING);
}

// the loop routine runs over and over again forever:
void loop() {
  digitalWrite(LED_BUILTIN, state);
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  if (state){
    if((long)(micros() - last_micros) >= debouncing_time * 1000) {
    }
    else{
      Serial.print(sensorValue);      //the first variable for plotting
      Serial.print(",");              //seperator
      Serial.println(sensorValue);          //the second variable for plotting including line break
    }
    
  }
  else{
    // print out the value you read:
    Serial.println(sensorValue);
    
  }
  
  //delay(1);        // delay in between reads for stability
}
void debounceInterrupt() {
  if((long)(micros() - last_micros) >= debouncing_time * 1000) {
    Interrupt();
    last_micros = micros();
  }
}
void Interrupt() {
  state = !state;
}