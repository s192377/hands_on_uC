#include <Servo.h>
Servo myServo;
const byte interruptPin = 7;
volatile byte state = LOW;
long debouncing_time = 15; //Debouncing Time in Milliseconds
volatile unsigned long last_micros;


void setup() {
  myServo.attach(6);
  pinMode(LED_BUILTIN, OUTPUT);
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), debounceInterrupt, RISING);
}

void loop() {
  digitalWrite(LED_BUILTIN, state);
  if(state){
    myServo.write(0);
  }
  else{
    myServo.write(90);
  }
}

void debounceInterrupt() {
  if((long)(micros() - last_micros) >= debouncing_time * 1000) {
    Interrupt();
    last_micros = micros();
  }
}
void Interrupt() {
  state = !state;
}
