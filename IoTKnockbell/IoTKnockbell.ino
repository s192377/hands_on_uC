// include the library code:
#include "arduino_secrets.h" // Arduino Cloud interface
#include "thingProperties.h" // Arduino Cloud interface
#include <LiquidCrystal.h> // LCD Display
#include "Timer5.h" // Timer control
#include <Servo.h> // Servo control
#include "pitches.h" // Melody notes
#include <RTCZero.h> // Real Time Clock
#include <SPI.h> // Push notification
#include <WiFi101.h> // Push notification

extern RTCZero rtc;

// notes in the melody:
int melody[] = {
  NOTE_C7, NOTE_G6, NOTE_G6, NOTE_A6, NOTE_G6, NOTE_G6, NOTE_B6, NOTE_C7
};
// note durations: 4 = quarter note, 8 = eighth note, etc.:
int noteDurations[] = {
  4, 8, 8, 4, 4, 4, 4, 4
};

// Used in Listen()
#define knockTimeLength 20 // Array length
#define knockLength 40 // Millisecons, time until ADC goes back to zero after nock
#define loudKnock 200 // Threshold to consider ADC data as a knock

// Use in Interruption rutines
#define debouncingTime 15 //Debouncing Time in Milliseconds

enum tasks { Waiting, Listening, Analyzing, Ringing, Notifying};

volatile tasks currentTask = Waiting;
Servo myServo;
// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 9, en = 10, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
String prev_msg_top;
String prev_msg_bottom;

// Interrupt variables
const byte interruptButton = 7; // Push Button interrupt
const byte interruptADC = 0; // ADC interrupt
const byte buzzer = 1; // Melody output pin
const byte servoPin = 6; // Servo pin
volatile byte state = LOW;
volatile unsigned long lastMicros;
volatile unsigned long riseInit;
volatile char InterruptADCFlag = false;
volatile char InterruptTimerFlag = false;
volatile char NotifyingFlag = false;

// Array to store when each nock has happened
int knockTime[knockTimeLength];
byte knockCounter = 0;
byte StdDevThreshold = 30; // Milliseconds
// IFTTT setup
const char *host = "maker.ifttt.com";
const char *privateKey = "cnj9-tgOpC5CO9H3nm9dDh";

// Notification LED
int ledPin = 11;           // the PWM pin the LED is attached to
int brightness = 0;    // how bright the LED is
int fadeAmount = 5;    // how many points to fade the LED by

// the setup routine runs once when you press reset:
void setup() {
  LCDPrintTop("Initializing...");
  // initialize serial communication at 9600 bits per second:
  Serial.begin(9600);
  // This delay gives the chance to wait for a Serial Monitor without blocking if none is found
  //delay(1500); 

  rtc.begin(); // initialize RTC

  // Defined in thingProperties.h
  initProperties();

  // Connect to Arduino IoT Cloud
  ArduinoCloud.begin(ArduinoIoTPreferredConnection);

  /*
     The following function allows you to obtain more information
     related to the state of network and IoT Cloud connection and errors
     the higher number the more granular information you’ll get.
     The default is 0 (only errors).
     Maximum is 4
 */
  setDebugMessageLevel(2);
  ArduinoCloud.printDebugInfo();
  ArduinoCloud.update();
  pinMode(LED_BUILTIN, OUTPUT);

  // PushButton Interruption
  //pinMode(buzzer, OUTPUT);
  pinMode(interruptButton, INPUT_PULLUP);
  pinMode(interruptADC, INPUT);
  pinMode(ledPin, OUTPUT);
  analogWrite(ledPin, 0); // Clear LED Notification
  attachInterrupt(digitalPinToInterrupt(interruptADC), debounceADC, RISING);
  attachInterrupt(digitalPinToInterrupt(interruptButton), debounceButton, RISING);


  // Control Servo
  myServo.attach(servoPin);

  // Timer initialization
  // Prescaler set to 2, interrupt every 2 seconds, TC_CTRLA_PRESCALER_DIV2 
  MyTimer5.begin(1);
  MyTimer5.attachInterrupt(InterruptTimer);
  MyTimer5.end();
  for (int thisNote = 0; thisNote < 8; thisNote++) {
    // to calculate the note duration, take one second divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(buzzer, melody[thisNote], noteDuration);
    
    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(buzzer);
  }
}

// the loop routine runs over and over again forever:
void loop() {
  // STATE MACHINE --------------------------------- START
  switch(currentTask){
    case Waiting:
      // Start listening to the ADC during the timer duration
      if (InterruptADCFlag){
        InterruptADCFlag = false;
        // Start Timer
        MyTimer5.begin(1);
        MyTimer5.start();
        currentTask = Listening;
      }
      else{
        Wait();
      }
      break;
    case Listening:
      if (InterruptTimerFlag){
        InterruptTimerFlag = false;
        // Stop listening to the ADC and change to Analyzing
        currentTask = Analyzing;
        // Reset timer
        MyTimer5.end();
      }
      else{
        Listen();
      }
      break;
    case Analyzing:
      int result = Analyze();
      if(result){
        // If knock sequence valid
        currentTask = Ringing;
      }
      else{
        // If knock sequence invalid
        currentTask = Waiting;
      }
      break;
    case Ringing:
      // Play melody
      Ring();
      currentTask = Notifying;
      break;
    case Notifying:
      // Notifiy user locally and remotely
      Notify();
      currentTask = Waiting;
      break;
  }
  // STATE MACHINE --------------------------------- END
  if (NotifyingFlag){
    // Toggle Notification LED
      NotificationLED();
    // Wait until the notification is cleared with the button
  }
  else{
    // Clear LED Notification
    analogWrite(ledPin, 0); 
  }
}

// State machine main functions
void Wait() {
  LCDPrintTop("Waiting...");
  ArduinoCloud.update();
  delay(1);
}
void Listen() {
  LCDPrintTop("Listening...");
  LCDPrintBottom(String(knockCounter));
  // read the input on analog pin 0:
  int sensorValue = analogRead(A0);
  // print out the value you read:
  //Serial.println(sensorValue);
  if (sensorValue > loudKnock){
    // Knock detected
    // Check if it is the same knock
    if((long)(micros() - riseInit) >= knockLength * 1000) {
      // Store the timer value and increment the knock counter
      if (knockCounter < knockTimeLength){
        // one count is 61us
        knockTime[knockCounter] = REG_TC3_COUNT16_COUNT;
        ++knockCounter;
      }
      riseInit = micros();
    }
    
  }
}

byte Analyze() {
  LCDPrintTop("Analazing...");
  int mean = knockTime[0];
  int i;
  // Single knocks are not valid
  if (knockCounter == 0){
    return false;
  }
  // Calculate the mean of the delta time between timestamps
  for (i = 1; i < knockCounter; ++i){
      mean += (knockTime[i]-knockTime[i-1]);
  }
  mean = mean/(knockCounter);
  Serial.println(mean);
  // Calculate the standard deviation of the delta time between timestamps
  float StdDev = pow(knockTime[0]- mean, 2);
  Serial.println(knockTime[0]);
  for(i = 1; i < knockCounter; ++i){
      Serial.println(knockTime[i]);
      StdDev += pow((knockTime[i]-knockTime[i-1]) - mean, 2);
  }
  // Convert the resulting standard deviation from raw ticks to milliseconds
  StdDev = sqrt(StdDev /(knockCounter))*0.061; // one tick is 61us
  Serial.println(StdDev);
  // Clear the data for the next run
  for (i = 0; i < knockTimeLength; ++i){
    knockTime[i] = 0;
  }
  knockCounter = 0;
  // Knocks are equally spaced
  if (StdDev<StdDevThreshold){
    return true;
  }
  // Knocks are not spaced equally
  else{
    return false;
  }
}

void Ring() {
  LCDPrintTop("Ringing...");
  // iterate over the notes of the melody:
  for (int thisNote = 0; thisNote < 8; thisNote++) {
    // to calculate the note duration, take one second divided by the note type.
    //e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    int noteDuration = 1000 / noteDurations[thisNote];
    tone(buzzer, melody[thisNote], noteDuration);
    
    // to distinguish the notes, set a minimum time between them.
    // the note's duration + 30% seems to work well:
    int pauseBetweenNotes = noteDuration * 1.30;
    delay(pauseBetweenNotes);
    // stop the tone playing:
    noTone(buzzer);
  }
}

void Notify(){
  NotifyingFlag = true;
  // Send IFTTT push notification
  send_event("knockAlert");
  // Send message to Arduino Cloud Dashboard
  String date = printDate()+printTime();
  knockAlert = String("Last knock at: ")+date;
  // Display on LCD
  lcd.begin(16, 2);
  lcd.print("Last knock at:");
  lcd.setCursor(0, 1);
  lcd.print(date);
}
void NotificationLED() {
  // set the brightness of pin 9:
  analogWrite(ledPin, brightness);

  // change the brightness for next time through the loop:
  brightness = brightness + fadeAmount;

  // reverse the direction of the fading at the ends of the fade:
  if (brightness <= 0 || brightness >= 255) {
    fadeAmount = -fadeAmount;
  }
  // wait for 30 milliseconds to see the dimming effect
  delay(30);
}
// Supporting Functions-----------------------------------------------------------

// Arduino Cloud Callback Functions ----------------
void onUnlockButtonChange() {
  // unlockButton is modified only from the web interface
  if(unlockButton){
    // Open
    myServo.write(0);
  }
  else{
    // Close
    myServo.write(90);
    NotifyingFlag = false;
  }
}
// Arduino Cloud Not Used Fuctions
void onLedChange() {
}
void onKnockAlertChange() {
}

// Interruption Callback Functions ----------------
// Debounce noise in external interruption input signals
void debounceButton() {
  if((long)(micros() - lastMicros) >= debouncingTime * 1000) {
    // Call the appropiate callback function
    InterruptButton();
    lastMicros = micros();
  }
}
void debounceADC() {
  if((long)(micros() - lastMicros) >= debouncingTime * 1000) {
    // Call the appropiate callback function
    InterruptADC();
    lastMicros = micros();
  }
}
// Callback functions use flags to keep the code to a minimum
void InterruptButton() {
   NotifyingFlag = false;
}
void InterruptADC() {
  // Only interrupt when needed
  if (currentTask == Waiting){
    InterruptADCFlag = true;
  }
}
void InterruptTimer() {
  // Only interrupt when needed
  if (currentTask == Listening){
    InterruptTimerFlag = true;
  }
}
// LCD Print Functions --------------------
// Get the real time and date using the RTC library and format to human readable strings
String printTime(){
  return String(rtc.getHours()+2)+String(":")+addZero(rtc.getMinutes()); // +String(":")+String(rtc.getSeconds())
}

String printDate(){
  return String(rtc.getDay())+String("/")+String(rtc.getMonth())+String("/")+String(rtc.getYear())+String(" ");
}

String addZero(int number) {
  if (number < 10) {
    return "0"+String(number); // print a 0 before if the number is < than 10
  }
  return String(number);
}

void LCDPrintTop(String msg){
  if (!NotifyingFlag){ // Dont overwrite the "Last knock message"
    if(msg != prev_msg_top){ // Only write the same message once to avoid flickering
      lcd.begin(16, 2);
      lcd.print(msg);
      prev_msg_top = msg;
    }
  }
}

void LCDPrintBottom(String msg){
  if (!NotifyingFlag){ // Dont overwrite the "Last knock message"
    if(msg != prev_msg_bottom){ // Only write the same message once to avoid flickering
      lcd.setCursor(0, 1);
      lcd.print(msg);
      prev_msg_bottom = msg;
    }
  }
}

// Push Notification Function --------------------
void send_event(const char *event){
  Serial.print("Connecting to ");
  Serial.println(host);
  
  // Use WiFiClient class to create TCP connections
  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("Connection failed");
    return;
  }
  
  // We now create a URI for the request
  String url = "/trigger/";
  url += event;
  url += "/with/key/";
  url += privateKey;
  
  Serial.print("Requesting URL: ");
  Serial.println(url);
  
  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" + 
               "Connection: close\r\n\r\n");

  // Read all the lines of the reply from server and print them to Serial,
  // the connection will close when the server has sent all the data.
  while(client.connected())
  {
    if(client.available())
    {
      String line = client.readStringUntil('\r');
      Serial.print(line);
    } else {
      // No data yet, wait a bit
      delay(50);
    };
  }
  // All done
  Serial.println();
  Serial.println("closing connection");

  client.stop();
}
