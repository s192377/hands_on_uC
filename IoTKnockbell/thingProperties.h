#include <ArduinoIoTCloud.h>
#include <Arduino_ConnectionHandler.h>


const char THING_ID[] = "524bd9f8-4c09-4134-b229-0785b5bb3d8f";

const char SSID[]     = SECRET_SSID;    // Network SSID (name)
const char PASS[]     = SECRET_PASS;    // Network password (use for WPA, or use as key for WEP)

void onKnockAlertChange();
void onLedChange();
void onUnlockButtonChange();

String knockAlert;
bool led;
bool unlockButton;

void initProperties(){

  ArduinoCloud.setThingId(THING_ID);
  ArduinoCloud.addProperty(knockAlert, READ, ON_CHANGE, onKnockAlertChange);
  ArduinoCloud.addProperty(led, READWRITE, ON_CHANGE, onLedChange);
  ArduinoCloud.addProperty(unlockButton, READWRITE, ON_CHANGE, onUnlockButtonChange);

}

WiFiConnectionHandler ArduinoIoTPreferredConnection(SSID, PASS);
