// include the library code:
#include <LiquidCrystal.h>
#include "Timer5.h"
// initialize the library by associating any needed LCD interface pin
// with the arduino pin number it is connected to
const int rs = 9, en = 10, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
volatile byte state = LOW;

void setup() {
  MyTimer5.begin(1);
  MyTimer5.attachInterrupt(Interrupt);
  MyTimer5.start();
  pinMode(LED_BUILTIN, OUTPUT);
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  // Print a message to the LCD.
  lcd.print("hello, world!");
}

void loop() {
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
  lcd.setCursor(0, 1);
  // print the number of seconds since reset:
  lcd.print(REG_TC4_COUNT16_COUNT);
  digitalWrite(LED_BUILTIN, state);
}
void Interrupt() {
  state = !state;
}
