#include "arduino_secrets.h"
/* 
  Sketch generated by the Arduino IoT Cloud Thing "IOTKnockBell"
  https://create.arduino.cc/cloud/things/524bd9f8-4c09-4134-b229-0785b5bb3d8f 

  Arduino IoT Cloud Properties description

  The following variables are automatically generated and updated when changes are made to the Thing properties

  bool led;

  Properties which are marked as READ/WRITE in the Cloud Thing will also have functions
  which are called when their values are changed from the Dashboard.
  These functions are generated with the Thing and added at the end of this sketch.
*/

#include "thingProperties.h"

void setup() {
  // Initialize serial and wait for port to open:
  Serial.begin(9600);
  // This delay gives the chance to wait for a Serial Monitor without blocking if none is found
  delay(1500); 

  // Defined in thingProperties.h
  initProperties();

  // Connect to Arduino IoT Cloud
  ArduinoCloud.begin(ArduinoIoTPreferredConnection);
  
  /*
     The following function allows you to obtain more information
     related to the state of network and IoT Cloud connection and errors
     the higher number the more granular information you’ll get.
     The default is 0 (only errors).
     Maximum is 4
 */
  setDebugMessageLevel(2);
  ArduinoCloud.printDebugInfo();
  pinMode(LED_BUILTIN, OUTPUT);
}

void loop() {
  ArduinoCloud.update();
  // Your code here 
  
  
}


void onLedChange() {
  digitalWrite(LED_BUILTIN, led);
    Serial.print("The light is ");
    if (led) {
        Serial.println("ON");
    } else {
        Serial.println("OFF");
    }
}



